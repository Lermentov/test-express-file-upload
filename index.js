'use strict';
const app = require('express')();
const fileUpload = require('express-fileupload');
const http = require('http').Server(app);

app.use(fileUpload());
app.set('view engine', 'pug');

app.get('/', function(req, res) {
    res.render('index.pug');
});

app.post('/upload', function(req, res) {
    if(Object.keys(req.files).length === 0)
        return res.status(400).send('No se cargaron archivos');

    let sampleFile = req.files.sampleFile;

    sampleFile.mv(`upload/${sampleFile.name}`, function(err) {
        if(err) {
            console.log(`Error moviendo el archivo cargado. ${err}`);
        }

        res.send('File uploaded!');
    });
});

http.listen(8082, '0.0.0.0');
