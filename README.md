## Testing express-fileupload.

Developed on node 8.12.0

Dependencies:
- express 4.16.4
- express-fileupload 1.1.1-alpha.2
- pug 2.0.3

URLs:
- Pug attributes: https://pugjs.org/language/attributes.html
- Express-fileupload Basic file upload: https://github.com/richardgirges/express-fileupload/tree/master/example#basic-file-upload


## Prueba utilizando express-fileupload.

Desarrollado en node 8.12.0

Dependencias:
- express 4.16.4
- express-fileupload 1.1.1-alpha.2
- pug 2.0.3

URLs:
- Atributos en Pug: https://pugjs.org/language/attributes.html
- Express-fileupload carga basica de un archivo: https://github.com/richardgirges/express-fileupload/tree/master/example#basic-file-upload

Project Avatar taken from [warszawianka](https://openclipart.org/user-detail/warszawianka) 